{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = {self,nixpkgs,flake-utils}:
    let my-package = 
          with import nixpkgs {system = "aarch64-linux";};
          stdenv.mkDerivation rec {
           name = "j-thoughts";
            src = self;
            buildInputs = [pkgs.gcc pkgs.makeWrapper];
            buildPhase = ''gcc -o main ./src/main.c'';
            installPhase = ''
            mkdir -p $out/bin;
            cp main $out/bin/j-thoughts
                        '';
            postFixup = ''wrapProgram $out/bin/j-thoughts   --add-flags "/home/vamshi/j-thoughts/"

             '';
         };
    in
      (flake-utils.lib.eachDefaultSystem (system: {
        
        packages.j-thoughts = my-package;
        defaultPackage =  self.packages.${system}.j-thoughts;
        apps.j-thoughts = flake-utils.lib.mkApp {
          drv = self.packages.${system}.j-thoughts;
        };
        apps.default = self.apps."${system}".j-thoughts;
             }));
}